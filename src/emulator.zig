///This struct describes a chip8 processor and it's supported IO (keypad, display, and audio)
const std = @import("std");
const RndGen = std.rand.DefaultPrng;
const SDL = @import("sdl.zig");
const Display = @import("emulator/display.zig");
const Audio = @import("emulator/audio.zig");

//==========================================
// Fields
//==========================================
display: Display,
audio: Audio,
v_regs: [16]u8,
pc: u16,
i_reg: u16,
sp: u8,
dt: u8,
st: u8,
mem: []u8,
stack: [16]u16,
keypad: [16]bool,
exit_flag: bool,
rand_gen: RndGen,

//==========================================
// Constants
//==========================================
const MEM_SIZE = 4096;
const PC_START = 0x200;

const Self = @This();

//==========================================
// Meta
//==========================================
pub fn init() !Self {
    //initialize memory
    const mem = try std.heap.page_allocator.alloc(u8, MEM_SIZE);
    errdefer std.heap.page_allocator.free(mem);
    load_sprites(mem) catch unreachable;

    //initialize IO
    var display = try Display.init();
    errdefer display.deinit();

    var audio = try Audio.init();
    errdefer audio.deinit();

    return Self{
        .v_regs = undefined,
        .pc = PC_START,
        .i_reg = undefined,
        .sp = 0,
        .dt = 0,
        .st = 0,
        .mem = mem,
        .stack = undefined,
        .display = display,
        .audio = audio,
        .keypad = [_]bool{false} ** 16,
        .exit_flag = false,
        .rand_gen = RndGen.init(@intCast(std.time.timestamp())),
    };
}

pub fn deinit(self: *Self) void {
    self.display.deinit();
    self.audio.deinit();
    std.heap.page_allocator.free(self.mem);
}

pub fn run_rom(self: *Self, name: []const u8) !void {
    try load_rom(name, self.mem);
    self.start_timers();
    try self.event_loop();
}

//==========================================
// Main Loop
//==========================================
fn event_loop(self: *Self) !void {
    var e: SDL.Event = undefined;

    while (self.exit_flag == false) {
        try self.exec_instruction();
        std.time.sleep(std.time.ns_per_ms / 2);
        while (SDL.poll_event(&e) != 0) {
            self.handle_events(e);
        }
    }
}

fn handle_events(self: *Self, event: SDL.Event) void {
    switch (event.type) {
        SDL.EVENT.QUIT => self.exit_flag = true,
        SDL.EVENT.KEYDOWN => {
            const key = event.key;
            if (key_to_hex(key.keysym.sym)) |hex| {
                self.keypad[hex] = true;
            }
        },
        SDL.EVENT.KEYUP => {
            const key = event.key;
            if (key_to_hex(key.keysym.sym)) |hex| {
                self.keypad[hex] = false;
            }
        },
        else => {},
    }
}

///function maps a key from the keyboard to a specific hex code
fn key_to_hex(key_code: i32) ?u4 {
    return switch (key_code) {
        SDL.KEY_CODE.K_1 => 0x1,
        SDL.KEY_CODE.K_2 => 0x2,
        SDL.KEY_CODE.K_3 => 0x3,
        SDL.KEY_CODE.K_4 => 0xC,
        SDL.KEY_CODE.K_q => 0x4,
        SDL.KEY_CODE.K_w => 0x5,
        SDL.KEY_CODE.K_e => 0x6,
        SDL.KEY_CODE.K_r => 0xD,
        SDL.KEY_CODE.K_a => 0x7,
        SDL.KEY_CODE.K_s => 0x8,
        SDL.KEY_CODE.K_d => 0x9,
        SDL.KEY_CODE.K_f => 0xE,
        SDL.KEY_CODE.K_z => 0xA,
        SDL.KEY_CODE.K_x => 0x0,
        SDL.KEY_CODE.K_c => 0xB,
        SDL.KEY_CODE.K_v => 0xF,
        else => null,
    };
}

fn exec_instruction(self: *Self) !void {
    const begin = self.mem[self.pc];
    const end = self.mem[self.pc + 1];
    const word: u16 = (@as(u16, begin) << 8) + @as(u16, end);

    if (word == 0) {
        self.exit_flag = true;
        return;
    }
    const prefix = begin >> 4;

    self.pc += 2;
    switch (prefix) {
        0x0 => switch (end) {
            0xE0 => { // CLS
                try self.display.clear_screen();
            },
            0xEE => { // RET
                if (self.sp == 0) {
                    return error.STACK_UNDERFLOW;
                }
                self.sp -= 1;
                self.pc = self.stack[self.sp];
            },
            else => {
                std.debug.print("error at {x}: invalid instruction: {x}\n", .{ self.pc, word });
            },
        },
        0x1 => {
            const addr = word & 0xFFF;

            self.pc = addr;
        }, // JP
        0x2 => {
            const addr = word & 0xFFF;

            if (self.sp == self.stack.len - 1) {
                return error.STACK_OVERFLOW;
            }
            self.stack[self.sp] = self.pc;
            self.sp += 1;
            self.pc = addr;
        }, // CALL
        0x3 => {
            const x = begin & 0xF;
            const byte = end;

            if (self.v_regs[x] == byte) {
                self.pc += 2;
            }
        }, // SE_VAL
        0x4 => {
            const x = begin & 0xF;
            const byte = end;

            if (self.v_regs[x] != byte) {
                self.pc += 2;
            }
        }, // SNE_VAL
        0x5 => {
            const x = begin & 0xF;
            const y = end >> 4;
            if (self.v_regs[x] == self.v_regs[y]) {
                self.pc += 2;
            }
        }, // SE_REG
        0x6 => {
            const x = begin & 0xF;
            const byte = end;

            self.v_regs[x] = byte;
        }, // LD_VAL
        0x7 => {
            const x = begin & 0xF;
            const byte = end;

            self.v_regs[x] +%= byte;
        }, // ADD_VAL
        0x8 => {
            const x = begin & 0xf;
            const y = end >> 4;
            const suffix = end & 0xf;
            switch (suffix) {
                0x0 => {
                    self.v_regs[x] = self.v_regs[y];
                }, // LD_REG
                0x1 => {
                    self.v_regs[x] |= self.v_regs[y];
                }, // OR
                0x2 => {
                    self.v_regs[x] &= self.v_regs[y];
                }, // AND
                0x3 => {
                    self.v_regs[x] ^= self.v_regs[y];
                }, // XOR
                0x4 => {
                    if (@as(u9, self.v_regs[x]) + @as(u9, self.v_regs[y]) > 255) {
                        self.v_regs[0xF] = 1;
                    } else {
                        self.v_regs[0xF] = 0;
                    }
                    self.v_regs[x] +%= self.v_regs[y];
                }, // ADD
                0x5 => {
                    if (self.v_regs[x] >= self.v_regs[y]) {
                        self.v_regs[0xF] = 1;
                    } else {
                        self.v_regs[0xF] = 0;
                    }
                    self.v_regs[x] -%= self.v_regs[y];
                }, // SUB
                0x6 => {
                    self.v_regs[0xF] = self.v_regs[x] % 2;
                    self.v_regs[x] /= 2;
                }, // SHR
                0x7 => {
                    if (self.v_regs[x] <= self.v_regs[y]) {
                        self.v_regs[0xF] = 1;
                    } else {
                        self.v_regs[0xF] = 0;
                    }
                    self.v_regs[x] = self.v_regs[y] -% self.v_regs[x];
                }, // SUBN
                0xE => {
                    self.v_regs[0xF] = self.v_regs[x] >> 7;
                    self.v_regs[x] = self.v_regs[x] * 2;
                }, // SHL
                else => {
                    std.debug.print("error at {x}: invalid instruction: {x}\n", .{ self.pc, word });
                },
            }
        },
        0x9 => {
            const x = begin & 0xF;
            const y = end >> 4;
            if (self.v_regs[x] != self.v_regs[y]) {
                self.pc += 2;
            }
        }, // SNE_REG
        0xa => {
            const addr = word & 0xFFF;
            self.i_reg = addr;
        }, //LD_I
        0xb => {
            const addr = word & 0xFFF;
            self.pc += addr;
        }, //JP_V0
        0xc => {
            const x = begin & 0xF;
            const byte = end;
            const random = self.rand_gen.random();
            const num = random.int(u8);
            self.v_regs[x] = num & byte;
        }, //RND
        0xd => {
            const x = begin & 0xF;
            const y = end >> 4;
            const n = end & 0xF;
            const result = try self.display.draw_sprite(self.mem[self.i_reg..].ptr, @intCast(n), self.v_regs[x], self.v_regs[y]);
            self.v_regs[0xF] = @intFromBool(result);
        }, //DRW
        0xe => {
            const x = begin & 0xF;
            switch (end) {
                0x9E => {
                    if (self.keypad[self.v_regs[x]]) {
                        self.pc += 2;
                    }
                }, //SKP
                0xA1 => {
                    if (!self.keypad[self.v_regs[x]]) {
                        self.pc += 2;
                    }
                }, //SKNP
                else => {
                    std.debug.print("error at {x}: invalid instruction: {x}\n", .{ self.pc, word });
                },
            }
        },
        0xf => {
            const x = begin & 0xf;
            switch (end) {
                0x07 => {
                    self.v_regs[x] = self.dt;
                }, //LD <- DT
                0x0A => {
                    var e: SDL.Event = undefined;
                    loop: while (true) {
                        while (SDL.poll_event(&e) != 0) {
                            switch (e.type) {
                                SDL.EVENT.QUIT => {
                                    self.exit_flag = true;
                                    break :loop;
                                },
                                SDL.EVENT.KEYDOWN => {
                                    const key = e.key;
                                    if (key_to_hex(key.keysym.sym)) |hex| {
                                        self.keypad[hex] = true;

                                        self.v_regs[x] = hex;
                                        break :loop;
                                    }
                                },
                                SDL.EVENT.KEYUP => {
                                    const key = e.key;
                                    if (key_to_hex(key.keysym.sym)) |hex| {
                                        self.keypad[hex] = false;

                                        self.v_regs[x] = hex;
                                        break :loop;
                                    }
                                },
                                else => {},
                            }
                        }
                    }
                }, //LD_K
                0x15 => {
                    self.dt = self.v_regs[x];
                }, //LD -> DT
                0x18 => {
                    self.st = self.v_regs[x];
                }, //LD -> ST
                0x1E => {
                    self.i_reg += self.v_regs[x];
                }, //ADD_I
                0x29 => {
                    self.i_reg = @as(u16, self.v_regs[x]) * 5;
                }, //LD sprite
                0x33 => {
                    self.mem[self.i_reg] = self.v_regs[x] / 100;
                    self.mem[self.i_reg + 1] = (self.v_regs[x] / 10) % 100;
                    self.mem[self.i_reg + 2] = self.v_regs[x] % 10;
                }, //BCD
                0x55 => {
                    for (0..x + 1) |i| {
                        self.mem[self.i_reg + i] = self.v_regs[i];
                    }
                }, //LD <- registers
                0x65 => {
                    for (0..x + 1) |i| {
                        self.v_regs[i] = self.mem[self.i_reg + i];
                    }
                }, //LD -> registers
                else => {
                    std.debug.print("error at {x}: invalid instruction: {x}\n", .{ self.pc, word });
                },
            }
        },
        else => undefined,
    }
}

//==========================================
// Time
//==========================================

fn start_timers(self: *Self) void {
    const Thread = std.Thread;
    var dt_thread = Thread.spawn(.{}, delay_timer, .{&self.dt}) catch @panic("unable to start thread");
    dt_thread.detach();
    var st_thread = Thread.spawn(.{}, sound_timer, .{ &self.st, &self.audio }) catch @panic("unable to start thread");
    st_thread.detach();
}

fn delay_timer(dt: *u8) void {
    while (true) {
        std.time.sleep(std.time.ns_per_s / 60);
        const AtomicOrder = std.builtin.AtomicOrder;
        const val = dt.*;
        if (val > 0) {
            //we only want to store `val - 1` in dt if `val` remains with the same value, otherwise we'd be overriding another value that was inputed somewhere else
            _ = @cmpxchgStrong(u8, dt, val, val - 1, AtomicOrder.Monotonic, AtomicOrder.Monotonic);
        }
    }
}

fn sound_timer(rt: *u8, audio: *Audio) void {
    var playing = false;
    while (true) {
        std.time.sleep(std.time.ns_per_s / 60);
        const AtomicOrder = std.builtin.AtomicOrder;
        const val = rt.*;
        const play = val != 0;
        if (play and @cmpxchgStrong(u8, rt, val, val - 1, AtomicOrder.Monotonic, AtomicOrder.Monotonic) == null and !playing) {
            audio.play();
            playing = true;
        } else if (!play and playing) {
            audio.pause();
            playing = false;
        }
    }
}

//==========================================
// Memory
//==========================================

fn load_sprites(mem: []u8) !void {
    if (mem.len < 0x1FF) {
        return error.MEM_OVERFLOW;
    }
    const digit_sprites = [_]u8{
        0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
        0x20, 0x60, 0x20, 0x20, 0x70, // 1
        0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
        0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
        0x90, 0x90, 0xF0, 0x10, 0x10, // 4
        0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
        0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
        0xF0, 0x10, 0x20, 0x40, 0x40, // 7
        0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
        0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
        0xF0, 0x90, 0xF0, 0x90, 0x90, // A
        0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
        0xF0, 0x80, 0x80, 0x80, 0xF0, // C
        0xE0, 0x90, 0x90, 0x90, 0xE0, // D
        0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
        0xF0, 0x80, 0xF0, 0x80, 0x80, // F
    };
    //pointer magic until zig implements pointer size conversions
    const addr: usize = @intFromPtr(&digit_sprites);
    std.mem.copyForwards(u8, mem, @as([*]u8, @ptrFromInt(addr))[0..80]);
}

fn load_rom(name: []const u8, mem: []u8) !void {
    const fs = std.fs;
    const file = try fs.cwd().openFile(name, .{});
    _ = try file.read(mem[PC_START..]);
}
