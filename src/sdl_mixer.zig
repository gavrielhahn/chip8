const c = @cImport({
    @cInclude("SDL2/SDL_mixer.h");
});

//Constants

pub const DEFAULT_FORMAT = c.MIX_DEFAULT_FORMAT;

//Types
pub const Chunk = struct {
    ptr: *c.Mix_Chunk,

    const Self = @This();
    pub fn deinit(self: Self) void {
        c.Mix_FreeChunk(self.ptr);
    }
    pub fn play_channel(self: Self, channel: c_int, loops: c_int) ?Channel {
        const channel_id = c.Mix_PlayChannel(channel, self.ptr, loops);
        if (channel_id == -1) {
            return null;
        }
        return .{ .id = channel_id };
    }
};

pub const Channel = struct {
    id: c_int,

    const Self = @This();
    pub fn pause(self: Self) void {
        pause_channel(self.id);
    }
    pub fn unpause(self: Self) void {
        resume_channel(self.id);
    }
    pub fn halt(self: Self) !void {
        if (halt_channel(self.id) < 0) {
            return error.SDL_mixer;
        }
    }
};

//Functions

pub fn open_audio(frequency: c_int, format: u16, channels: c_int, chunksize: c_int) !void {
    if (c.Mix_OpenAudio(frequency, format, channels, chunksize) < 0) {
        return error.SDL_mixer;
    }
}

pub fn load_WAV(file: [*c]const u8) ?Chunk {
    const ptr = c.Mix_LoadWAV(file);
    if (ptr == null) {
        return null;
    }
    return .{ .ptr = ptr };
}
pub const pause_channel = c.Mix_Pause;
pub const resume_channel = c.Mix_Resume;
pub const halt_channel = c.Mix_HaltChannel;

pub const get_error = c.Mix_GetError;
pub const quit = c.Mix_Quit;
