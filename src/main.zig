const std = @import("std");
const Emulator = @import("emulator.zig");
const SDL = @import("sdl.zig");

pub fn main() !void {
    //initialize sdl
    try SDL.init(SDL.INIT.VIDEO | SDL.INIT.AUDIO);
    defer SDL.deinit();

    var args = std.process.args();
    defer args.deinit();

    const proc_name = args.next() orelse unreachable;
    const rom_name = args.next() orelse {
        std.log.err("usage: {s} <rom_name>\n", .{proc_name});
        return error.INVALID_USAGE;
    };
    var emu = try Emulator.init();
    defer emu.deinit();

    try emu.run_rom(rom_name);
}
