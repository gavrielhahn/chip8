pub const c = @cImport({
    @cInclude("SDL2/SDL.h");
});

//  # conventions to follow:
//  * General guidelines: stay as close to the original api as possible(following zig naming conventions(including init and deinit) and removing unnecessary SDL prefix)
//
//  ## Structs
//  * c structs will be wrapped with structs with similar names, the field containing the struct will be named `raw` and will point to it with a `*` pointer
//  * mentioned wrappers will contain all the supported methods for the given struct,
//    each function in the SDL api which accecpts a pointer to the struct as the first parameter is considerd a struct method
//
//  ## Constants
//  * constants that can be devided into sub catagories will be given their own "struct" which will be named in all caps as well (e.g. SDL_INIT_TIMER will be accecible as INIT.TIMER)
//
//  ## Functions
//  * functions that do not count as "struct methods" will not be changed except for their name
//  * functions will check for errors and convert them to zig errors, logging the relevant SDL error
//
//  ## API
//  * all unimplemented parts of the api will be exposed through the `c` constant

// Constants
pub const INIT = struct {
    pub const TIMER = c.SDL_INIT_TIMER;
    pub const AUDIO = c.SDL_INIT_AUDIO;
    pub const VIDEO = c.SDL_INIT_VIDEO;
    pub const JOYSTICK = c.SDL_INIT_JOYSTICK;
    pub const HAPTIC = c.SDL_INIT_HAPTIC;
    pub const GAMECONTROLLER = c.SDL_INIT_GAMECONTROLLER;
    pub const EVENTS = c.SDL_INIT_EVENTS;
    pub const EVERYTHING = c.SDL_INIT_EVERYTHING;
    pub const NOPARACHUTE = c.SDL_INIT_NOPARACHUTE;
};

pub const WINDOWPOS = struct {
    pub const UNDEFINED = c.SDL_WINDOWPOS_UNDEFINED;
    pub const CENTERED = c.SDL_WINDOWPOS_CENTERED;
};

pub const WINDOW = struct {
    pub const FULLSCREEN = c.SDL_WINDOW_FULLSCREEN;
    pub const OPENGL = c.SDL_WINDOW_OPENGL;
    pub const SHOWN = c.SDL_WINDOW_SHOWN;
    pub const HIDDEN = c.SDL_WINDOW_HIDDEN;
    pub const BORDERLESS = c.SDL_WINDOW_BORDERLESS;
    pub const RESIZABLE = c.SDL_WINDOW_RESIZABLE;
    pub const MINIMIZED = c.SDL_WINDOW_MINIMIZED;
    pub const MAXIMIZED = c.SDL_WINDOW_MAXIMIZED;
    pub const MOUSE_GRABBED = c.SDL_WINDOW_MOUSE_GRABBED;
    pub const INPUT_FOCUS = c.SDL_WINDOW_INPUT_FOCUS;
    pub const MOUSE_FOCUS = c.SDL_WINDOW_MOUSE_FOCUS;
    pub const FULLSCREEN_DESKTOP = c.SDL_WINDOW_FULLSCREEN_DESKTOP;
    pub const FOREIGN = c.SDL_WINDOW_FOREIGN;
    pub const ALLOW_HIGHDPI = c.SDL_WINDOW_ALLOW_HIGHDPI;
    pub const MOUSE_CAPTURE = c.SDL_WINDOW_MOUSE_CAPTURE;
    pub const ALWAYS_ON_TOP = c.SDL_WINDOW_ALWAYS_ON_TOP;
    pub const SKIP_TASKBAR = c.SDL_WINDOW_SKIP_TASKBAR;
    pub const UTILITY = c.SDL_WINDOW_UTILITY;
    pub const TOOLTIP = c.SDL_WINDOW_TOOLTIP;
    pub const POPUP_MENU = c.SDL_WINDOW_POPUP_MENU;
    pub const KEYBOARD_GRABBED = c.SDL_WINDOW_KEYBOARD_GRABBED;
    pub const VULKAN = c.SDL_WINDOW_VULKAN;
    pub const METAL = c.SDL_WINDOW_METAL;
    pub const INPUT_GRABBED = c.SDL_WINDOW_INPUT_GRABBED;
};

pub const EVENT = struct {
    pub const FIRSTEVENT = c.SDL_FIRSTEVENT;
    pub const QUIT = c.SDL_QUIT;
    pub const APP_TERMINATING = c.SDL_APP_TERMINATING;
    pub const APP_LOWMEMORY = c.SDL_APP_LOWMEMORY;
    pub const APP_WILLENTERBACKGROUND = c.SDL_APP_WILLENTERBACKGROUND;
    pub const APP_DIDENTERBACKGROUND = c.SDL_APP_DIDENTERBACKGROUND;
    pub const APP_WILLENTERFOREGROUND = c.SDL_APP_WILLENTERFOREGROUND;
    pub const APP_DIDENTERFOREGROUND = c.SDL_APP_DIDENTERFOREGROUND;
    pub const LOCALECHANGED = c.SDL_LOCALECHANGED;
    pub const DISPLAYEVENT = c.SDL_DISPLAYEVENT;
    pub const WINDOWEVENT = c.SDL_WINDOWEVENT;
    pub const SYSWMEVENT = c.SDL_SYSWMEVENT;
    pub const KEYDOWN = c.SDL_KEYDOWN;
    pub const KEYUP = c.SDL_KEYUP;
    pub const TEXTEDITING = c.SDL_TEXTEDITING;
    pub const TEXTINPUT = c.SDL_TEXTINPUT;
    pub const KEYMAPCHANGED = c.SDL_KEYMAPCHANGED;
    pub const TEXTEDITING_EXT = c.SDL_TEXTEDITING_EXT;
    pub const MOUSEMOTION = c.SDL_MOUSEMOTION;
    pub const MOUSEBUTTONDOWN = c.SDL_MOUSEBUTTONDOWN;
    pub const MOUSEBUTTONUP = c.SDL_MOUSEBUTTONUP;
    pub const MOUSEWHEEL = c.SDL_MOUSEWHEEL;
    pub const JOYAXISMOTION = c.SDL_JOYAXISMOTION;
    pub const JOYBALLMOTION = c.SDL_JOYBALLMOTION;
    pub const JOYHATMOTION = c.SDL_JOYHATMOTION;
    pub const JOYBUTTONDOWN = c.SDL_JOYBUTTONDOWN;
    pub const JOYBUTTONUP = c.SDL_JOYBUTTONUP;
    pub const JOYDEVICEADDED = c.SDL_JOYDEVICEADDED;
    pub const JOYDEVICEREMOVED = c.SDL_JOYDEVICEREMOVED;
    pub const JOYBATTERYUPDATED = c.SDL_JOYBATTERYUPDATED;
    pub const CONTROLLERAXISMOTION = c.SDL_CONTROLLERAXISMOTION;
    pub const CONTROLLERBUTTONDOWN = c.SDL_CONTROLLERBUTTONDOWN;
    pub const CONTROLLERBUTTONUP = c.SDL_CONTROLLERBUTTONUP;
    pub const CONTROLLERDEVICEADDED = c.SDL_CONTROLLERDEVICEADDED;
    pub const CONTROLLERDEVICEREMOVED = c.SDL_CONTROLLERDEVICEREMOVED;
    pub const CONTROLLERDEVICEREMAPPED = c.SDL_CONTROLLERDEVICEREMAPPED;
    pub const CONTROLLERTOUCHPADDOWN = c.SDL_CONTROLLERTOUCHPADDOWN;
    pub const CONTROLLERTOUCHPADMOTION = c.SDL_CONTROLLERTOUCHPADMOTION;
    pub const CONTROLLERTOUCHPADUP = c.SDL_CONTROLLERTOUCHPADUP;
    pub const CONTROLLERSENSORUPDATE = c.SDL_CONTROLLERSENSORUPDATE;
    pub const FINGERDOWN = c.SDL_FINGERDOWN;
    pub const FINGERUP = c.SDL_FINGERUP;
    pub const FINGERMOTION = c.SDL_FINGERMOTION;
    pub const DOLLARGESTURE = c.SDL_DOLLARGESTURE;
    pub const DOLLARRECORD = c.SDL_DOLLARRECORD;
    pub const MULTIGESTURE = c.SDL_MULTIGESTURE;
    pub const CLIPBOARDUPDATE = c.SDL_CLIPBOARDUPDATE;
    pub const DROPFILE = c.SDL_DROPFILE;
    pub const DROPTEXT = c.SDL_DROPTEXT;
    pub const DROPBEGIN = c.SDL_DROPBEGIN;
    pub const DROPCOMPLETE = c.SDL_DROPCOMPLETE;
    pub const AUDIODEVICEADDED = c.SDL_AUDIODEVICEADDED;
    pub const AUDIODEVICEREMOVED = c.SDL_AUDIODEVICEREMOVED;
    pub const SENSORUPDATE = c.SDL_SENSORUPDATE;
    pub const RENDER_TARGETS_RESET = c.SDL_RENDER_TARGETS_RESET;
    pub const RENDER_DEVICE_RESET = c.SDL_RENDER_DEVICE_RESET;
    pub const POLLSENTINEL = c.SDL_POLLSENTINEL;
    pub const USEREVENT = c.SDL_USEREVENT;
    pub const LASTEVENT = c.SDL_LASTEVENT;
};

pub const KEY_CODE = struct {
    pub const K_UNKNOWN = c.SDLK_UNKNOWN;
    pub const K_RETURN = c.SDLK_RETURN;
    pub const K_ESCAPE = c.SDLK_ESCAPE;

    pub const K_BACKSPACE = c.SDLK_BACKSPACE;
    pub const K_TAB = c.SDLK_TAB;
    pub const K_SPACE = c.SDLK_SPACE;
    pub const K_EXCLAIM = c.SDLK_EXCLAIM;
    pub const K_QUOTEDBL = c.SDLK_QUOTEDBL;
    pub const K_HASH = c.SDLK_HASH;
    pub const K_PERCENT = c.SDLK_PERCENT;
    pub const K_DOLLAR = c.SDLK_DOLLAR;
    pub const K_AMPERSAND = c.SDLK_AMPERSAND;
    pub const K_QUOTE = c.SDLK_QUOTE;
    pub const K_LEFTPAREN = c.SDLK_LEFTPAREN;
    pub const K_RIGHTPAREN = c.SDLK_RIGHTPAREN;
    pub const K_ASTERISK = c.SDLK_ASTERISK;
    pub const K_PLUS = c.SDLK_PLUS;
    pub const K_COMMA = c.SDLK_COMMA;
    pub const K_MINUS = c.SDLK_MINUS;
    pub const K_PERIOD = c.SDLK_PERIOD;
    pub const K_SLASH = c.SDLK_SLASH;
    pub const K_0 = c.SDLK_0;
    pub const K_1 = c.SDLK_1;
    pub const K_2 = c.SDLK_2;
    pub const K_3 = c.SDLK_3;
    pub const K_4 = c.SDLK_4;
    pub const K_5 = c.SDLK_5;
    pub const K_6 = c.SDLK_6;
    pub const K_7 = c.SDLK_7;
    pub const K_8 = c.SDLK_8;
    pub const K_9 = c.SDLK_9;
    pub const K_COLON = c.SDLK_COLON;
    pub const K_SEMICOLON = c.SDLK_SEMICOLON;
    pub const K_LESS = c.SDLK_LESS;
    pub const K_EQUALS = c.SDLK_EQUALS;
    pub const K_GREATER = c.SDLK_GREATER;
    pub const K_QUESTION = c.SDLK_QUESTION;
    pub const K_AT = c.SDLK_AT;
    pub const K_LEFTBRACKET = c.SDLK_LEFTBRACKET;
    pub const K_BACKSLASH = c.SDLK_BACKSLASH;
    pub const K_RIGHTBRACKET = c.SDLK_RIGHTBRACKET;
    pub const K_CARET = c.SDLK_CARET;
    pub const K_UNDERSCORE = c.SDLK_UNDERSCORE;
    pub const K_BACKQUOTE = c.SDLK_BACKQUOTE;
    pub const K_a = c.SDLK_a;
    pub const K_b = c.SDLK_b;
    pub const K_c = c.SDLK_c;
    pub const K_d = c.SDLK_d;
    pub const K_e = c.SDLK_e;
    pub const K_f = c.SDLK_f;
    pub const K_g = c.SDLK_g;
    pub const K_h = c.SDLK_h;
    pub const K_i = c.SDLK_i;
    pub const K_j = c.SDLK_j;
    pub const K_k = c.SDLK_k;
    pub const K_l = c.SDLK_l;
    pub const K_m = c.SDLK_m;
    pub const K_n = c.SDLK_n;
    pub const K_o = c.SDLK_o;
    pub const K_p = c.SDLK_p;
    pub const K_q = c.SDLK_q;
    pub const K_r = c.SDLK_r;
    pub const K_s = c.SDLK_s;
    pub const K_t = c.SDLK_t;
    pub const K_u = c.SDLK_u;
    pub const K_v = c.SDLK_v;
    pub const K_w = c.SDLK_w;
    pub const K_x = c.SDLK_x;
    pub const K_y = c.SDLK_y;
    pub const K_z = c.SDLK_z;
    pub const K_CAPSLOCK = c.SDLK_CAPSLOCK;
    pub const K_F1 = c.SDLK_F1;
    pub const K_F2 = c.SDLK_F2;
    pub const K_F3 = c.SDLK_F3;
    pub const K_F4 = c.SDLK_F4;
    pub const K_F5 = c.SDLK_F5;
    pub const K_F6 = c.SDLK_F6;
    pub const K_F7 = c.SDLK_F7;
    pub const K_F8 = c.SDLK_F8;
    pub const K_F9 = c.SDLK_F9;
    pub const K_F10 = c.SDLK_F10;
    pub const K_F11 = c.SDLK_F11;
    pub const K_F12 = c.SDLK_F12;
    pub const K_PRINTSCREEN = c.SDLK_PRINTSCREEN;
    pub const K_SCROLLLOCK = c.SDLK_SCROLLLOCK;
    pub const K_PAUSE = c.SDLK_PAUSE;
    pub const K_INSERT = c.SDLK_INSERT;
    pub const K_HOME = c.SDLK_HOME;
    pub const K_PAGEUP = c.SDLK_PAGEUP;
    pub const K_DELETE = c.SDLK_DELETE;
    pub const K_END = c.SDLK_END;
    pub const K_PAGEDOWN = c.SDLK_PAGEDOWN;
    pub const K_RIGHT = c.SDLK_RIGHT;
    pub const K_LEFT = c.SDLK_LEFT;
    pub const K_DOWN = c.SDLK_DOWN;
    pub const K_UP = c.SDLK_UP;
    pub const K_NUMLOCKCLEAR = c.SDLK_NUMLOCKCLEAR;
    pub const K_KP_DIVIDE = c.SDLK_KP_DIVIDE;
    pub const K_KP_MULTIPLY = c.SDLK_KP_MULTIPLY;
    pub const K_KP_MINUS = c.SDLK_KP_MINUS;
    pub const K_KP_PLUS = c.SDLK_KP_PLUS;
    pub const K_KP_ENTER = c.SDLK_KP_ENTER;
    pub const K_KP_1 = c.SDLK_KP_1;
    pub const K_KP_2 = c.SDLK_KP_2;
    pub const K_KP_3 = c.SDLK_KP_3;
    pub const K_KP_4 = c.SDLK_KP_4;
    pub const K_KP_5 = c.SDLK_KP_5;
    pub const K_KP_6 = c.SDLK_KP_6;
    pub const K_KP_7 = c.SDLK_KP_7;
    pub const K_KP_8 = c.SDLK_KP_8;
    pub const K_KP_9 = c.SDLK_KP_9;
    pub const K_KP_0 = c.SDLK_KP_0;
    pub const K_KP_PERIOD = c.SDLK_KP_PERIOD;
    pub const K_APPLICATION = c.SDLK_APPLICATION;
    pub const K_POWER = c.SDLK_POWER;
    pub const K_KP_EQUALS = c.SDLK_KP_EQUALS;
    pub const K_F13 = c.SDLK_F13;
    pub const K_F14 = c.SDLK_F14;
    pub const K_F15 = c.SDLK_F15;
    pub const K_F16 = c.SDLK_F16;
    pub const K_F17 = c.SDLK_F17;
    pub const K_F18 = c.SDLK_F18;
    pub const K_F19 = c.SDLK_F19;
    pub const K_F20 = c.SDLK_F20;
    pub const K_F21 = c.SDLK_F21;
    pub const K_F22 = c.SDLK_F22;
    pub const K_F23 = c.SDLK_F23;
    pub const K_F24 = c.SDLK_F24;
    pub const K_EXECUTE = c.SDLK_EXECUTE;
    pub const K_HELP = c.SDLK_HELP;
    pub const K_MENU = c.SDLK_MENU;
    pub const K_SELECT = c.SDLK_SELECT;
    pub const K_STOP = c.SDLK_STOP;
    pub const K_AGAIN = c.SDLK_AGAIN;
    pub const K_UNDO = c.SDLK_UNDO;
    pub const K_CUT = c.SDLK_CUT;
    pub const K_COPY = c.SDLK_COPY;
    pub const K_PASTE = c.SDLK_PASTE;
    pub const K_FIND = c.SDLK_FIND;
    pub const K_MUTE = c.SDLK_MUTE;
    pub const K_VOLUMEUP = c.SDLK_VOLUMEUP;
    pub const K_VOLUMEDOWN = c.SDLK_VOLUMEDOWN;
    pub const K_KP_COMMA = c.SDLK_KP_COMMA;
    pub const K_KP_EQUALSAS400 = c.SDLK_KP_EQUALSAS400;
    pub const K_ALTERASE = c.SDLK_ALTERASE;
    pub const K_SYSREQ = c.SDLK_SYSREQ;
    pub const K_CANCEL = c.SDLK_CANCEL;
    pub const K_CLEAR = c.SDLK_CLEAR;
    pub const K_PRIOR = c.SDLK_PRIOR;
    pub const K_RETURN2 = c.SDLK_RETURN2;
    pub const K_SEPARATOR = c.SDLK_SEPARATOR;
    pub const K_OUT = c.SDLK_OUT;
    pub const K_OPER = c.SDLK_OPER;
    pub const K_CLEARAGAIN = c.SDLK_CLEARAGAIN;
    pub const K_CRSEL = c.SDLK_CRSEL;
    pub const K_EXSEL = c.SDLK_EXSEL;
    pub const K_KP_00 = c.SDLK_KP_00;
    pub const K_KP_000 = c.SDLK_KP_000;
    pub const K_THOUSANDSSEPARATOR = c.SDLK_THOUSANDSSEPARATOR;
    pub const K_DECIMALSEPARATOR = c.SDLK_DECIMALSEPARATOR;
    pub const K_CURRENCYUNIT = c.SDLK_CURRENCYUNIT;
    pub const K_CURRENCYSUBUNIT = c.SDLK_CURRENCYSUBUNIT;
    pub const K_KP_LEFTPAREN = c.SDLK_KP_LEFTPAREN;
    pub const K_KP_RIGHTPAREN = c.SDLK_KP_RIGHTPAREN;
    pub const K_KP_LEFTBRACE = c.SDLK_KP_LEFTBRACE;
    pub const K_KP_RIGHTBRACE = c.SDLK_KP_RIGHTBRACE;
    pub const K_KP_TAB = c.SDLK_KP_TAB;
    pub const K_KP_BACKSPACE = c.SDLK_KP_BACKSPACE;
    pub const K_KP_A = c.SDLK_KP_A;
    pub const K_KP_B = c.SDLK_KP_B;
    pub const K_KP_C = c.SDLK_KP_C;
    pub const K_KP_D = c.SDLK_KP_D;
    pub const K_KP_E = c.SDLK_KP_E;
    pub const K_KP_F = c.SDLK_KP_F;
    pub const K_KP_XOR = c.SDLK_KP_XOR;
    pub const K_KP_POWER = c.SDLK_KP_POWER;
    pub const K_KP_PERCENT = c.SDLK_KP_PERCENT;
    pub const K_KP_LESS = c.SDLK_KP_LESS;
    pub const K_KP_GREATER = c.SDLK_KP_GREATER;
    pub const K_KP_AMPERSAND = c.SDLK_KP_AMPERSAND;
    pub const K_KP_DBLAMPERSAND = c.SDLK_KP_DBLAMPERSAND;
    pub const K_KP_VERTICALBAR = c.SDLK_KP_VERTICALBAR;
    pub const K_KP_DBLVERTICALBAR = c.SDLK_KP_DBLVERTICALBAR;
    pub const K_KP_COLON = c.SDLK_KP_COLON;
    pub const K_KP_HASH = c.SDLK_KP_HASH;
    pub const K_KP_SPACE = c.SDLK_KP_SPACE;
    pub const K_KP_AT = c.SDLK_KP_AT;
    pub const K_KP_EXCLAM = c.SDLK_KP_EXCLAM;
    pub const K_KP_MEMSTORE = c.SDLK_KP_MEMSTORE;
    pub const K_KP_MEMRECALL = c.SDLK_KP_MEMRECALL;
    pub const K_KP_MEMCLEAR = c.SDLK_KP_MEMCLEAR;
    pub const K_KP_MEMADD = c.SDLK_KP_MEMADD;
    pub const K_KP_MEMSUBTRACT = c.SDLK_KP_MEMSUBTRACT;
    pub const K_KP_MEMMULTIPLY = c.SDLK_KP_MEMMULTIPLY;
    pub const K_KP_MEMDIVIDE = c.SDLK_KP_MEMDIVIDE;
    pub const K_KP_PLUSMINUS = c.SDLK_KP_PLUSMINUS;
    pub const K_KP_CLEAR = c.SDLK_KP_CLEAR;
    pub const K_KP_CLEARENTRY = c.SDLK_KP_CLEARENTRY;
    pub const K_KP_BINARY = c.SDLK_KP_BINARY;
    pub const K_KP_OCTAL = c.SDLK_KP_OCTAL;
    pub const K_KP_DECIMAL = c.SDLK_KP_DECIMAL;
    pub const K_KP_HEXADECIMAL = c.SDLK_KP_HEXADECIMAL;
    pub const K_LCTRL = c.SDLK_LCTRL;
    pub const K_LSHIFT = c.SDLK_LSHIFT;
    pub const K_LALT = c.SDLK_LALT;
    pub const K_LGUI = c.SDLK_LGUI;
    pub const K_RCTRL = c.SDLK_RCTRL;
    pub const K_RSHIFT = c.SDLK_RSHIFT;
    pub const K_RALT = c.SDLK_RALT;
    pub const K_RGUI = c.SDLK_RGUI;
    pub const K_MODE = c.SDLK_MODE;
    pub const K_AUDIONEXT = c.SDLK_AUDIONEXT;
    pub const K_AUDIOPREV = c.SDLK_AUDIOPREV;
    pub const K_AUDIOSTOP = c.SDLK_AUDIOSTOP;
    pub const K_AUDIOPLAY = c.SDLK_AUDIOPLAY;
    pub const K_AUDIOMUTE = c.SDLK_AUDIOMUTE;
    pub const K_MEDIASELECT = c.SDLK_MEDIASELECT;
    pub const K_WWW = c.SDLK_WWW;
    pub const K_MAIL = c.SDLK_MAIL;
    pub const K_CALCULATOR = c.SDLK_CALCULATOR;
    pub const K_COMPUTER = c.SDLK_COMPUTER;
    pub const K_AC_SEARCH = c.SDLK_AC_SEARCH;
    pub const K_AC_HOME = c.SDLK_AC_HOME;
    pub const K_AC_BACK = c.SDLK_AC_BACK;
    pub const K_AC_FORWARD = c.SDLK_AC_FORWARD;
    pub const K_AC_STOP = c.SDLK_AC_STOP;
    pub const K_AC_REFRESH = c.SDLK_AC_REFRESH;
    pub const K_AC_BOOKMARKS = c.SDLK_AC_BOOKMARKS;
    pub const K_BRIGHTNESSDOWN = c.SDLK_BRIGHTNESSDOWN;
    pub const K_BRIGHTNESSUP = c.SDLK_BRIGHTNESSUP;
    pub const K_DISPLAYSWITCH = c.SDLK_DISPLAYSWITCH;
    pub const K_KBDILLUMTOGGLE = c.SDLK_KBDILLUMTOGGLE;
    pub const K_KBDILLUMDOWN = c.SDLK_KBDILLUMDOWN;
    pub const K_KBDILLUMUP = c.SDLK_KBDILLUMUP;
    pub const K_EJECT = c.SDLK_EJECT;
    pub const K_SLEEP = c.SDLK_SLEEP;
    pub const K_APP1 = c.SDLK_APP1;
    pub const K_APP2 = c.SDLK_APP2;
    pub const K_AUDIOREWIND = c.SDLK_AUDIOREWIND;
    pub const K_AUDIOFASTFORWARD = c.SDLK_AUDIOFASTFORWARD;
    pub const K_SOFTLEFT = c.SDLK_SOFTLEFT;
    pub const K_SOFTRIGHT = c.SDLK_SOFTRIGHT;
    pub const K_CALL = c.SDLK_CALL;
    pub const K_ENDCALL = c.SDLK_ENDCALL;
};

pub const AUDIO_ALLOW = struct {
    pub const ANY_CHANGE = c.SDL_AUDIO_ALLOW_ANY_CHANGE;
    pub const FREQUENCY_CHANGE = c.SDL_AUDIO_ALLOW_FREQUENCY_CHANGE;
    pub const FORMAT_CHANGE = c.SDL_AUDIO_ALLOW_FORMAT_CHANGE;
    pub const CHANNELS_CHANGE = c.SDL_AUDIO_ALLOW_CHANNELS_CHANGE;
    pub const SAMPLES_CHANGE = c.SDL_AUDIO_ALLOW_SAMPLES_CHANGE;
};
// Types

pub const Window = struct {
    raw: *c.SDL_Window,

    const Self = @This();
    pub fn get_window_surface(self: *Self) Surface {
        return Surface{ .raw = c.SDL_GetWindowSurface(self.raw) };
    }
    pub fn update_window_surface(self: *Self) !void {
        if (c.SDL_UpdateWindowSurface(self.raw) < 0) {
            return error.SDL;
        }
    }
    pub fn deinit(self: *Self) void {
        c.SDL_DestroyWindow(self.raw);
    }
};
pub const Surface = struct {
    raw: *c.SDL_Surface,

    const Self = @This();
    pub fn fill_rect(self: *Self, rect: [*c]const c.SDL_Rect, color: u32) !void {
        if (c.SDL_FillRect(self.raw, rect, color) < 0) {
            return error.SDL;
        }
    }
};

pub const Event = c.SDL_Event;

pub const AudioSpec = c.SDL_AudioSpec;

pub const AudioDevice = struct {
    id: c.SDL_AudioDeviceID,

    const Self = @This();
    pub fn open(device: [*c]const u8, iscapture: c_int, desired: *const AudioSpec, obtained: ?*AudioSpec, allowed_changes: c_int) ?Self {
        const id = c.SDL_OpenAudioDevice(device, iscapture, desired, obtained, allowed_changes);
        if (id == 0) {
            return null;
        }
        return Self{ .id = id };
    }
    pub fn pause(self: Self, pause_on: c_int) void {
        c.SDL_PauseAudioDevice(self.id, pause_on);
    }
    pub fn queue_audio(self: Self, data: *const anyopaque, len: u32) !void {
        if (c.SDL_QueueAudio(self.id, data, len) < 0) {
            return error.SDL;
        }
    }
    pub fn close(self: Self) void {
        c.SDL_CloseAudioDevice(self.id);
    }
};

pub const Rect = c.SDL_Rect;

// Functions
pub fn init(flags: u32) !void {
    if (c.SDL_Init(flags) < 0) {
        return error.SDL;
    }
}
pub const deinit = c.SDL_Quit;
pub const get_error = c.SDL_GetError;

pub fn create_window(title: [*c]const u8, x: c_int, y: c_int, w: c_int, h: c_int, flags: u32) ?Window {
    const ptr = c.SDL_CreateWindow(title, x, y, w, h, flags);
    if (ptr == null) {
        return null;
    }
    return Window{ .raw = @ptrCast(ptr) };
}

pub const map_rgb = c.SDL_MapRGB;
pub const poll_event = c.SDL_PollEvent;
pub fn load_WAV(file: [*c]const u8, spec: *AudioSpec, audio_buf: *[*c]u8, audio_len: *u32) !void {
    if (c.SDL_LoadWAV(file, spec, audio_buf, audio_len) == 0) {
        return error.SDL;
    }
}
pub const free_WAV = c.SDL_FreeWAV;
