/// This struct manages the chip8 display system
const SDL = @import("../sdl.zig");
window: SDL.Window,
screen: [WIDTH * HEIGHT]bool,
black: u32,
white: u32,

// display
const WIDTH = 64;
const HEIGHT = 32;
const SCALE = 20;
const WINDOW_WIDTH = SCALE * WIDTH;
const WINDOW_HEIGHT = SCALE * HEIGHT;

const Self = @This();

//TODO: implement a managable display api

pub fn init() !Self {

    //initialize window
    var window = SDL.create_window(
        "chip-8",
        SDL.WINDOWPOS.CENTERED,
        SDL.WINDOWPOS.CENTERED,
        WINDOW_WIDTH,
        WINDOW_HEIGHT,
        SDL.WINDOW.SHOWN,
    ) orelse return error.SDL;

    //color window
    var surface = window.get_window_surface();
    const black = SDL.map_rgb(surface.raw.format, 0x00, 0x00, 0x00);
    const white = SDL.map_rgb(surface.raw.format, 0xFF, 0xFF, 0xFF);

    try surface.fill_rect(null, black);
    try window.update_window_surface();
    return Self{
        .window = window,
        .screen = [_]bool{false} ** (WIDTH * HEIGHT),
        .black = black,
        .white = white,
    };
}

/// draws a sprite on the screen with a constant width of 8
///
/// # Params
/// - `sprite`: a pointer to the sequence of bits representing the sprite
/// - `len`: the length of the array `sprite`, also the height of the sprite
/// - `x`: the x coordinate of the sprite on the screen
/// - `y`: the y coordinate of the sprite on the screen
pub fn draw_sprite(self: *Self, sprite: [*]const u8, len: u4, x: u8, y: u8) !bool {
    const BYTE_WIDTH = 8;
    var collision = false;
    for (0..@as(u8, len) * BYTE_WIDTH) |i| {
        //determine coordinates
        const x_offset = i % BYTE_WIDTH;
        const y_offset = i / BYTE_WIDTH;
        const x_cord = (x + x_offset) % WIDTH;
        const y_cord = (y + y_offset) % HEIGHT;
        const index = x_cord + y_cord * WIDTH;

        //determine xor results
        const sprite_val = (sprite[y_offset] >> @intCast(BYTE_WIDTH - x_offset - 1)) % 2 == 1;
        const screen_val = self.screen[index];
        collision = collision or (sprite_val and screen_val);
        const xor = sprite_val != screen_val;
        self.screen[index] = xor;

        //update screen
        var color: u32 = undefined;
        if (xor) {
            color = self.white;
        } else {
            color = self.black;
        }
        const rect = SDL.Rect{
            .x = @intCast(x_cord * SCALE),
            .y = @intCast(y_cord * SCALE),
            .w = SCALE,
            .h = SCALE,
        };
        var surface = self.window.get_window_surface();
        try surface.fill_rect(&rect, color);
    }
    try self.window.update_window_surface();
    return collision;
}

pub fn clear_screen(self: *Self) !void {
    var surface = self.window.get_window_surface();
    try surface.fill_rect(null, self.black);
    self.screen = [_]bool{false} ** (WIDTH * HEIGHT);
}

pub fn deinit(self: *Self) void {
    self.window.deinit();
}
