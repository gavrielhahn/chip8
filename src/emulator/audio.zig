/// This struct manages the chip8 audio system
const SDL_mixer = @import("../sdl_mixer.zig");

beep: SDL_mixer.Chunk,
channel: SDL_mixer.Channel,

const Self = @This();
const BEEP_PATH = "sound/beep.wav";

pub fn init() !Self {
    try SDL_mixer.open_audio(44100, SDL_mixer.DEFAULT_FORMAT, 2, 2048);
    const beep = SDL_mixer.load_WAV(BEEP_PATH) orelse return error.FILE_NOT_FOUND;

    return .{
        .beep = beep,
        .channel = undefined,
    };
}
pub fn play(self: *Self) void {
    self.channel = self.beep.play_channel(-1, -1) orelse .{ .id = -1 };
}

pub fn pause(self: *Self) void {
    self.channel.halt() catch {};
}

pub fn deinit(self: *Self) void {
    self.beep.deinit();
    SDL_mixer.quit();
}
