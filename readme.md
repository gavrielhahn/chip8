# Chip-8 emulator

This is an emulator for the chip-8 architectute written in zig

## Installation

build and install with [zig](https://ziglang.org)

```bash
git clone https://gitlab.com/gavrielhahn/chip8
cd chip8
zig build
```

## Usage
this emulator supports the basic chip-8 instructions set, example binaries can be found [here](https://github.com/kripod/chip8-roms)

```bash
zig build run -- <path-to-binary>
```
